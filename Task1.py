#!/usr/bin/env python3
"""Task 1 from page 50"""
import typing


class Vehicle:
    """Vehicle class"""
    def __init__(self, name: str, max_speed: typing.Union[int, float], color: str):
        """Ctor that takes 3 parameters name, max_speed, color"""
        self.name = name
        self.max_speed = max_speed
        self.color = color


class Car(Vehicle):
    """Car class"""
    def __init__(self, name: str, max_speed: typing.Union[int, float], color: str):
        """Ctor that takes 3 parameters """
        super().__init__(name, max_speed, color)
    pass


car: Car = Car("BMW", 300, "Black")
print("This " + car.color + " " + car.name + " has max speed " + str(car.max_speed) + " km/h")
