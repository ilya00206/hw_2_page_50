#!/usr/bin/env python3
"""Task 3 from page 50"""


class Unit:
    """Unit class"""
    def __init__(self, unit_name: str, unit_health: int):
        """Ctor that takes 2 parameters name and health of unit"""
        self.name: str = unit_name
        self.health: int = unit_health

    def __str__(self):
        """String representation of object"""
        return "Unit name: " + self.name + "\nHealth: " + str(self.health)

    def attack(self) -> None:
        """Simple method for attack"""
        print("Unit attacks")


class Soldier(Unit):
    def __init__(self):
        """Ctor that takes 2 parameters name and health of unit"""
        super().__init__("Soldier", 100)

    def __str__(self):
        """String representation of object"""
        return super().__str__()

    def attack(self) -> None:
        """Simple method for attack"""
        print("Soldier attacks")


soldier = Soldier()
print(soldier)
soldier.attack()
