#!/usr/bin/env python3
"""Task 2 from page 50"""


class Human:
    """Human abstraction class"""
    def walk(self):
        """Method that simulates walking"""
        print("I can walk")

    def sleep(self):
        """Method that simulates sleeping"""
        print("I can sleep")

    def eat(self):
        """Method that simulates eating"""
        print("I can eat")


class Child(Human):
    """Child abstraction class"""
    pass


tom: Child = Child()
tom.eat()
tom.walk()
tom.sleep()
